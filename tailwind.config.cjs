/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./src/**/*.{html,js,svelte,ts}'],
  theme: {
    colors: {
      "White": "#ffffff",
      "Black": "#000000",
      "Transparent": "#ffffff",
      "Orange": "#f9771c",
      "Charcoal": "#2a2824",
      "Pearl": "#efece5",
      "green": "#61704b",
      "Sage": "#c3cbb6",
      "blue": "#465c66",
      "Sky": "#bac6d4",
      "Sand": "#cab6a5"
    },
    extend: {},
  },
  plugins: [],
}
